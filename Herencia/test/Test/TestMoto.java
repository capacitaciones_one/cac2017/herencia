package Test;

import entidades.Moto;

public class TestMoto {
    public static void main(String[] args) {
        System.out.println("[...] Test Moto");
        Moto miMoto = new Moto();
        miMoto.color="Rojo";
        miMoto.marca="Yamaha";
        miMoto.modelo="FZ";
        miMoto.precio=50000;
        miMoto.setRompeViento(true);
        
        
        System.out.println("[...] Imprimierdo moto atributo por atributo");
        System.out.println(miMoto.color + " " + miMoto.marca + " " + miMoto.modelo + " " + miMoto.precio + " " + miMoto.isRompeViento());
        System.out.println("[OK] Imprimierdo moto atributo por atributo");
        
        System.out.println("[...] Imprimiendo dato de moto (objeto)");
        System.out.println(miMoto);
        System.out.println("[OK] Imprimiendo dato de moto (objeto)");
        
        System.out.println("[OK]");
    }
    
}
